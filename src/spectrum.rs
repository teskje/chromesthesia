use crate::sample;
use as_slice::AsSlice;
use core::f32::consts::PI;
use microfft::Complex32;
use micromath::F32Ext;

pub struct Buf([f32; sample::SIZE]);

impl Buf {
    pub const fn new() -> Self {
        Self([0.; sample::SIZE])
    }
}

pub struct Analyzer {
    buf: &'static mut Buf,
}

impl Analyzer {
    pub fn new(buf: &'static mut Buf) -> Self {
        Self { buf }
    }

    pub fn analyze(&mut self, samples: &sample::Buf) -> Spectrum {
        self.preprocess(samples);
        let fft_out = microfft::real::rfft_512(&mut self.buf.0);
        Spectrum::from_fft_output(fft_out)
    }

    fn preprocess(&mut self, samples: &sample::Buf) {
        let samples = samples.as_slice();
        let sum = samples.iter().fold(0., |acc, &s| acc + f32::from(s));
        let mean = sum / sample::SIZE as f32;

        for (i, &sample) in samples.iter().enumerate() {
            // remove DC
            let value = f32::from(sample) - mean;
            // apply Hann window
            self.buf.0[i] = value * hann(i);
        }
    }
}

fn hann(n: usize) -> f32 {
    0.5 * (1. - (2. * PI * n as f32 / sample::SIZE as f32).cos())
}

pub struct Spectrum<'s> {
    data: &'s [Complex32],
}

impl<'s> Spectrum<'s> {
    fn from_fft_output(fft_out: &'s [Complex32]) -> Self {
        let data = &fft_out[1..];
        Self { data }
    }

    pub fn into_amplitudes(self) -> Amplitudes<'s> {
        Amplitudes {
            spectrum: self,
            idx: 0,
        }
    }
}

pub struct Amplitudes<'s> {
    spectrum: Spectrum<'s>,
    idx: usize,
}

impl<'s> Iterator for Amplitudes<'s> {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        let c = self.spectrum.data.get(self.idx)?;
        let amp = c.norm_sqr().sqrt();
        self.idx += 1;
        Some(amp as u32)
    }
}

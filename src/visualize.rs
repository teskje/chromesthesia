use crate::{
    led::{self, Leds},
    spectrum::Spectrum,
    ws2812::Rgb,
};
use core::cmp;
use micromath::F32Ext;

type Amplitudes = [u32; led::COLUMNS];

const MIN_AMP: u32 = 2000;
const MAX_AMP: u32 = 20000;

pub struct Visualizer {
    leds: Leds,
    amps: Amplitudes,
}

impl Visualizer {
    pub fn new(leds: Leds) -> Self {
        Self {
            leds,
            amps: [0; led::COLUMNS],
        }
    }

    pub fn update(&mut self, spectrum: Spectrum) {
        self.update_amps(spectrum);

        let colors = ColorGen::new(&self.amps);
        self.leds.display(colors);

        self.degrade_amps();
    }

    fn update_amps(&mut self, spectrum: Spectrum) {
        let iter = spectrum.into_amplitudes();
        let new_amps = merge_amplitudes(iter);

        for i in 0..self.amps.len() {
            self.amps[i] = cmp::max(self.amps[i], new_amps[i]);
        }
    }

    fn degrade_amps(&mut self) {
        const DEGRADE_RATE: f32 = 0.08;

        for i in 0..self.amps.len() {
            let degraded = self.amps[i] as f32 * (1. - DEGRADE_RATE);
            self.amps[i] = degraded as u32;
        }
    }
}

fn merge_amplitudes<I>(mut iter: I) -> Amplitudes
where
    I: Iterator<Item = u32>,
{
    const X_MIN_BINS: usize = 5;
    const X_LOG_STEP: f32 = 1.5;

    let mut merged = [0; led::COLUMNS];

    for col in 0..merged.len() {
        let bin_count = X_MIN_BINS + X_LOG_STEP.powf(col as f32) as usize;
        let amps = iter.by_ref().take(bin_count);
        let squared_sum: u32 = amps.map(|a| a * a).sum();
        let rms = (squared_sum as f32 / bin_count as f32).sqrt();
        merged[col] = cmp::min(rms as u32, MAX_AMP);
    }

    merged
}

struct ColorGen<'a> {
    amps: &'a Amplitudes,
    col: usize,
    row: usize,
}

impl<'a> ColorGen<'a> {
    fn new(amps: &'a Amplitudes) -> Self {
        Self {
            amps,
            col: 0,
            row: 0,
        }
    }

    fn next_row(&mut self) {
        self.row += 1;

        if self.row >= led::ROWS {
            self.col += 1;
            self.row = 0;
        }
    }

    fn gen_color(&self) -> Rgb {
        const MIN_BRIGHTNESS: f32 = 0.05;

        let (r, g, b) = match self.col {
            0 => (0x80, 0x00, 0x80),
            1 => (0x25, 0x00, 0xdb),
            2 => (0x00, 0x00, 0xff),
            3 => (0x00, 0x25, 0xdb),
            4 => (0x00, 0x80, 0x80),
            5 => (0x00, 0xdb, 0x25),
            6 => (0x00, 0xff, 0x00),
            7 => (0x25, 0xdb, 0x00),
            8 => (0x80, 0x80, 0x00),
            9 => (0xdb, 0x25, 0x00),
            10 => (0xff, 0x00, 0x00),
            _ => unreachable!(),
        };

        let max_row = (led::ROWS - 1) as f32;
        let brightness = match self.row as f32 / max_row {
            b if b >= MIN_BRIGHTNESS => b,
            _ => MIN_BRIGHTNESS,
        };

        let dim = |c| (c as f32 * brightness) as u8;
        let (r, g, b) = (dim(r), dim(g), dim(b));

        Rgb::new(r, g, b)
    }
}

impl Iterator for ColorGen<'_> {
    type Item = Rgb;

    fn next(&mut self) -> Option<Self::Item> {
        const Y_STEP: u32 = (MAX_AMP - MIN_AMP) / led::ROWS as u32;

        if self.col >= led::COLUMNS {
            return None;
        }

        // The matrix is wired bottom-right to top-left. So to display low
        // frequencies at the left, we need to go backwards through the
        // amplitudes.
        let amp = self.amps[led::COLUMNS - self.col - 1];

        let y_threshold = MIN_AMP + Y_STEP * self.row as u32;

        let color = if amp >= y_threshold {
            self.gen_color()
        } else {
            Rgb::default()
        };

        self.next_row();
        Some(color)
    }
}

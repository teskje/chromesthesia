use crate::ws2812::{Rgb, Ws2812};
use as_slice::{AsMutSlice, AsSlice};
use hal::{
    dma::{self, dma1::C3},
    pac::SPI1,
    spi::Spi,
};

pub const COLUMNS: usize = 11;
pub const ROWS: usize = 18;
const COUNT: usize = COLUMNS * ROWS;

const BUF_SIZE: usize = COUNT * 24;

pub struct Buf([u8; BUF_SIZE]);

impl Buf {
    pub const fn new() -> Self {
        Self([0; BUF_SIZE])
    }
}

impl AsSlice for Buf {
    type Element = u8;

    fn as_slice(&self) -> &[u8] {
        &self.0
    }
}

impl AsMutSlice for Buf {
    fn as_mut_slice(&mut self) -> &mut [u8] {
        &mut self.0
    }
}

pub struct Leds {
    transfer: Option<dma::Transfer<Buf, C3, Spi<SPI1>>>,
}

impl Leds {
    pub fn init(spi: Spi<SPI1>, dma_channel: C3, buf: &'static mut Buf) -> Self {
        let spi_dma = spi.into_tx_dma(dma_channel);
        let transfer = spi_dma.start(buf);
        Self {
            transfer: Some(transfer),
        }
    }

    pub fn display<C>(&mut self, colors: C)
    where
        C: Iterator<Item = Rgb>,
    {
        let transfer = self.transfer.take().unwrap();

        // ensure this wait doesn't block
        assert!(transfer.is_complete());
        let (buf, dma_channel, spi) = transfer.wait();

        let mut ws2812 = Ws2812::new(buf);
        for color in colors {
            ws2812.write_color(color);
        }

        let spi_dma = spi.into_tx_dma(dma_channel);
        let transfer = ws2812.flush(spi_dma);

        self.transfer = Some(transfer);
    }
}

use as_slice::AsMutSlice;
use hal::{
    dma::{self, dma1::C3},
    pac::SPI1,
    spi::{Spi, SpiTxDma},
};

#[derive(Clone, Copy, Debug)]
pub struct Rgb {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

impl Rgb {
    pub fn new(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b }
    }
}

impl Default for Rgb {
    fn default() -> Self {
        Self::new(0, 0, 0)
    }
}

pub struct Ws2812<BUF>
where
    BUF: 'static,
{
    buf: &'static mut BUF,
    idx: usize,
}

// The initial implementation held two LED bits in a single byte, making it
// twice as memory efficient. Unfortunately, it produced slightly wrong colors,
// probably due to timing issues.
const PATTERNS: [u8; 2] = [
    0b1100_0000, // 0
    0b1111_1100, // 1
];

impl<BUF> Ws2812<BUF>
where
    BUF: AsMutSlice<Element = u8>,
{
    pub fn new(buf: &'static mut BUF) -> Self {
        let slice = buf.as_mut_slice();
        for b in slice.iter_mut() {
            *b = PATTERNS[0];
        }

        Self { buf, idx: 0 }
    }

    pub fn write_color(&mut self, color: Rgb) {
        self.write_byte(color.g);
        self.write_byte(color.r);
        self.write_byte(color.b);
    }

    fn write_byte(&mut self, mut byte: u8) {
        for _ in 0..8 {
            let bit = byte >> 7;
            self.write_bit(bit);
            byte <<= 1;
        }
    }

    fn write_bit(&mut self, bit: u8) {
        let slice = self.buf.as_mut_slice();
        slice[self.idx] = PATTERNS[bit as usize];
        self.idx += 1;
    }

    pub fn flush(self, spi_dma: SpiTxDma<SPI1, C3>) -> dma::Transfer<BUF, C3, Spi<SPI1>> {
        spi_dma.start(self.buf)
    }
}

#![no_std]
#![no_main]

#[cfg(not(feature = "semihosting"))]
extern crate panic_halt;
#[cfg(feature = "semihosting")]
extern crate panic_semihosting;

mod led;
mod sample;
mod spectrum;
mod visualize;
mod ws2812;

use hal::{adc::Adc, prelude::*, spi::Spi, time::Hertz, timer::Timer};
use led::Leds;
use sample::Sampler;
use visualize::Visualizer;

const SYSCLK: Hertz = Hertz(72_000_000);
const HCLK: Hertz = SYSCLK;
const PCLK1: Hertz = Hertz(HCLK.0 / 2);
const PCLK2: Hertz = HCLK;

/// Frequency of the SPI that to drives the LEDs.
const LED_FREQ: Hertz = Hertz(PCLK2.0 / 16);

#[rtic::app(device = hal::pac, peripherals = true)]
const APP: () = {
    struct Resources {
        sampler: Sampler,
        analyzer: spectrum::Analyzer,
        visualizer: Visualizer,
    }

    #[init]
    fn init(cx: init::Context) -> init::LateResources {
        static mut SAMPLE_BUF: [sample::Buf; 2] = [sample::Buf::new(), sample::Buf::new()];
        static mut SPECTRUM_BUF: spectrum::Buf = spectrum::Buf::new();
        static mut LED_BUF: led::Buf = led::Buf::new();

        let mut device = cx.device;

        let mut flash = device.FLASH.constrain();
        let mut rcc = device.RCC.constrain();
        let clocks = rcc
            .cfgr
            .use_hse(8.mhz())
            .sysclk(SYSCLK)
            .hclk(HCLK)
            .pclk1(PCLK1)
            .pclk2(PCLK2)
            .freeze(&mut flash.acr);

        let gpioa = device.GPIOA.split(&mut rcc.ahb);
        let dma = device.DMA1.split(&mut rcc.ahb);

        let sample_timer = Timer::tim6(device.TIM6, clocks, &mut rcc.apb1);

        let mut adc = Adc::adc1(device.ADC1, &mut device.ADC1_2, clocks, &mut rcc.ahb).enable();
        let adc_in = gpioa.pa0.into_analog();
        adc.set_channel(adc_in);

        let sck = gpioa.pa5.into_af5();
        let miso = gpioa.pa6.into_af5();
        let mosi = gpioa.pa7.into_af5();
        let spi = Spi::spi1(
            device.SPI1,
            (sck, miso, mosi),
            LED_FREQ,
            clocks,
            &mut rcc.apb2,
        );

        let analyzer = spectrum::Analyzer::new(SPECTRUM_BUF);
        let leds = Leds::init(spi, dma.ch3, LED_BUF);
        let visualizer = Visualizer::new(leds);

        let sampler = Sampler::start(sample_timer, adc, dma.ch1, SAMPLE_BUF);

        init::LateResources {
            sampler,
            analyzer,
            visualizer,
        }
    }

    #[task(binds = DMA1_CH1, priority = 3,
           resources = [sampler, analyzer, visualizer])]
    fn visualize_audio(cx: visualize_audio::Context) {
        let visualize_audio::Resources {
            sampler,
            analyzer,
            visualizer,
        } = cx.resources;

        let spectrum = sampler.process(|buf| analyzer.analyze(buf));
        visualizer.update(spectrum);
    }
};

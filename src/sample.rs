use as_slice::{AsMutSlice, AsSlice};
use hal::{
    adc::{self, Adc},
    dma::{self, dma1::C1},
    pac::{ADC1, TIM6},
    time::Hertz,
    timer::{self, Timer},
};

/// Rate at which audio samples are taken.
///
/// The maximum frequency that can be detected is `SAMPLE_RATE / 2`.
/// Setting this to a value > `40kHz` allows us to detect the whole range
/// of frequencies the microphone supports (20Hz - 20kHz) and avoid aliasing
/// effects.
const RATE: Hertz = Hertz(44_100);

/// Number of samples that go into the spectrum analysis.
///
/// The number of detected frequency bands is `SAMPLE_SIZE / 2`.
pub const SIZE: usize = 512;

pub struct Buf([u16; SIZE]);

impl Buf {
    pub const fn new() -> Self {
        Self([0; SIZE])
    }
}

impl AsSlice for Buf {
    type Element = u16;

    fn as_slice(&self) -> &[u16] {
        &self.0
    }
}

impl AsMutSlice for Buf {
    fn as_mut_slice(&mut self) -> &mut [u16] {
        &mut self.0
    }
}

type StoppedAdc = Adc<ADC1, adc::Stopped>;
type StartedAdc = Adc<ADC1, adc::Started>;

pub struct Sampler {
    transfer: dma::CircularTransfer<Buf, C1, StartedAdc>,
}

impl Sampler {
    pub fn start(
        mut timer: Timer<TIM6>,
        mut adc: StoppedAdc,
        mut dma_channel: C1,
        buf: &'static mut [Buf; 2],
    ) -> Self {
        // send timer update events to ADC
        timer.listen(timer::Event::Update);
        timer.set_master_mode(timer::Trgo::Update);

        // enable DMA interrupts for half and full transfer
        dma_channel.listen(dma::Event::HalfTransfer);
        dma_channel.listen(dma::Event::TransferComplete);

        adc.set_external_trigger(&timer);
        let adc_dma = adc.into_dma(dma_channel);

        let transfer = adc_dma.start_circular(buf);
        timer.start(RATE);

        Self { transfer }
    }

    pub fn process<F, R>(&mut self, f: F) -> R
    where
        F: FnOnce(&Buf) -> R,
    {
        self.transfer.peek(f).expect("sampling error")
    }
}
